# Bash, Git, BitBucket Intro (markdown)

Bash is the linux language to interact with the operating system, we do this via the terminal.

Git is version control

BitBucket is where gits go to hangout online

Markdown is what im typing right now

This is a lesson to introduce the follown technologies that we'll be using:

- git
- bitbucket
- bash
- markdown (.md)

### Bash

Bash (Bourne Again Shell) is the free and enhanced version of the Bourne shell distributed with Linux and GNU operating systems. Bash is similar to the original, but has added features such as command-line editing.

```
# Change directory
$ cd <folder>

# Go back
$ cd .. 

# List files to go into
$ ls

# remove directory
$ rmdir <folder>

# remove file
$ rm <file>

# check where you are located
$ pwd

# create file
$ touch <file name>


```

### Git

 Git is tool used for code management and version control
```
 Main commands for git: 
 - start a git repo using `git init`
 - add code for committing `git add .`
 - commit code with comment of what chnages you made `git commit -m`
 - check the status of git `git status`
 - check previous commits and comments `git log`
 - view where code is being uploaded `git remote --v`
 - push code onto repository `git push`
 - connect local repo to bitbucket `git remote add origin your-remote-url` 
```

### Bitbucket

 BitBucket is a git repository management solution designed for proffesional teams.

 - Create a repository 
 - link local repo with the one on bitbucket
 - commit and push regularly on the repo to develop a clear timeline
 - Can share repos with other parties 
 - Can keep repos private


# Header 1
## Header 2
### Header 3

- Bulleted
- List

1. Numbered
2. List

**Bold** and _Italic_ and `Code` text

[Link](url) and ![Image](src)

*single asterisks*
_single underscores_
**double asterisks**
__double underscores__

Emphasis can be used in the mi\*dd\*le of a word.

[ Text for the link ](URL)

This is [an example][id] reference-style link.
[id]: http://example.com/  "Optional Title Here"

![Alt text](/path/to/img.jpg "Optional title")

`span of code`


```python

def wiki_rocks(text):
    formatter = lambda t: "funky"+t
    return formatter(text)
```

> This is a blockquote with two paragraphs. 
> 
> Second paragraph.